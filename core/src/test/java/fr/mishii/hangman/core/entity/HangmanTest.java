package fr.mishii.hangman.core.entity;

import fr.mishii.hangman.core.entity.exception.GivenWordSizeException;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

class HangmanTest {
    @Test
    void getCurrentWord_whenGameCreated_shouldGetOnlyUnderscoresWithGuessWordSize() {
        var game = new Hangman("Toto", 6);

        var result = game.getCurrentWord();

        assertThat(result).isEqualTo("____");
    }

    @Test
    void canGuess_whenAttemptIsMoreThanZero_shouldReturnTrue() {
        var attempt = 1;
        var game = new Hangman("tati", attempt);

        assertThat(game.canGuess()).isTrue();
    }

    @Test
    void canGuess_whenAttemptIsZeroOrLess_shouldReturnFalse() {
        var attempt = 0;
        var game = new Hangman("Tonton", attempt);

        assertThat(game.canGuess()).isFalse();
    }

    @Test
    void isGuessed_whenCurrentWordHaveAtLeastOneUnderscore_shouldReturnFalse() {
        var attempt = 1;
        var game = new Hangman("Tati", attempt);

        game.guessIfOneCharacterIsInWord('T');
        game.guessIfOneCharacterIsInWord('a');

        assertThat(game.getCurrentWord()).isEqualTo("TAT_");
        assertThat(game.isGuessed()).isFalse();
    }

    @Test
    void isGuessed_whenCurrentWordNotHaveUnderscoreAndIsEqualToWordToGuess_shouldReturnTrue() {
        var attempt = 1;
        var game = new Hangman("Tati", attempt);

        game.guessIfOneCharacterIsInWord('T');
        game.guessIfOneCharacterIsInWord('a');
        game.guessIfOneCharacterIsInWord('i');

        assertThat(game.getCurrentWord()).isEqualTo("TATI");
        assertThat(game.isGuessed()).isTrue();
    }

    @Test
    void updateCurrentWord_givenOneCharacterPresentInWordToGuess_shouldReplaceUnderscoreInConcernedPlace() {
        var game = new Hangman("Tati", 6);

        game.guessIfOneCharacterIsInWord('a');

        assertThat(game.getCurrentWord()).isEqualTo("_A__");
    }

    @Test
    void updateCurrentWord_givenOneCharacterNotPresentInWordToGuess_shouldNotChangeCurrentWord() {
        var attempt = 6;
        var game = new Hangman("Tonton", attempt);
        assertThat(game.getCurrentWord()).isEqualTo("______");

        game.guessIfOneCharacterIsInWord('z');

        assertThat(game.getCurrentWord()).isEqualTo("______");
    }

    @Test
    void updateCurrentWord_givenOneCharacterNotPresentInWordToGuess_shouldReduceToOneTheAttemptNumber() {
        var attempt = 6;
        var game = new Hangman("Tonton", attempt);

        game.guessIfOneCharacterIsInWord('z');

        assertThat(game.getAttempt()).isEqualTo(attempt - 1);
    }

    @Test
    void guessWord_givenNotTheWordToGuess_shouldReduceAttemptNumber() throws Exception {
        var attempt = 5;
        var game = new Hangman("Tests", attempt);

        game.guessWord("testo");

        assertThat(game.getAttempt()).isEqualTo(attempt - 1);
    }

    @Test
    void guessWord_givenNotTheWordToGuess_shouldNotUpdateCurrentWord() throws Exception {
        var game = new Hangman("tata", 2);
        var currentWord = game.getCurrentWord();

        game.guessWord("tati");

        assertThat(game.getCurrentWord()).isEqualTo(currentWord);
    }

    @Test
    void guessWord_givenWordDifferentLengthThanWordToGuess_shouldThrow() {
        var game = new Hangman("tata", 2);
        assertThatThrownBy(() -> game.guessWord("tataa"))
                .isInstanceOf(GivenWordSizeException.class)
                .hasMessage("The given word is not the same size than to word to guess");
    }

    @Test
    void guessWord_givenWordEqualToWordToGuess_shouldUpdateCurrentWord() throws Exception {
        var game = new Hangman("Tonton", 2);

        game.guessWord("Tonton");

        assertThat(game.isGuessed()).isTrue();
    }

    @Test
    void guessWord_shouldNotBeCaseSensitive() throws Exception {
        var game = new Hangman("Tonton", 2);

        game.guessWord("tonton");

        assertThat(game.isGuessed()).isTrue();
    }
}