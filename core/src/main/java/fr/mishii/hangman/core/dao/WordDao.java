package fr.mishii.hangman.core.dao;

import java.io.IOException;
import java.util.List;

public interface WordDao {
    List<String> getWords(int numberWords) throws IOException;
}
