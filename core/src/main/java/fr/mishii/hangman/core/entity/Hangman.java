package fr.mishii.hangman.core.entity;

import fr.mishii.hangman.core.entity.exception.GivenWordSizeException;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Hangman {
    private final String wordToGuess;
    private String currentWord;
    private int attempt;

    public Hangman(String wordToGuess, int attempt) {
        this.wordToGuess = wordToGuess.toUpperCase();
        this.attempt = attempt;
        this.currentWord = createUnderscoreCurrentWord(wordToGuess);
    }

    private String createUnderscoreCurrentWord(String wordToGuess) {
        return Stream.iterate(0, n -> n + 1)
                .limit(wordToGuess.length())
                .map(n -> "_")
                .collect(Collectors.joining());
    }

    public String getCurrentWord() {
        return currentWord;
    }

    public String getWordToGuess() {
        return wordToGuess;
    }

    public int getAttempt() {
        return attempt;
    }

    public void guessIfOneCharacterIsInWord(char oneCharacter) {
        AtomicBoolean isChanged = new AtomicBoolean(false);
        char uppercaseChar = Character.toUpperCase(oneCharacter);

        updateCurrentWordDependOnWordToGuessAndGivenCharacter(isChanged, uppercaseChar);

        reduceAttemptIfCurrentWordChanged(isChanged);
    }

    private void updateCurrentWordDependOnWordToGuessAndGivenCharacter(AtomicBoolean isChanged, char uppercaseChar) {
        IntStream.range(0, wordToGuess.length())
                .forEach(index -> {
                    if (wordToGuess.charAt(index) == uppercaseChar) {
                        updateOneCharacterOfCurrentWord(uppercaseChar, index);
                        isChanged.set(true);
                    }
                });
    }

    private void updateOneCharacterOfCurrentWord(char oneCharacter, int index) {
        StringBuilder flexibleCurrentWord = new StringBuilder(currentWord);
        flexibleCurrentWord.setCharAt(index, oneCharacter);
        currentWord = flexibleCurrentWord.toString();
    }

    private void reduceAttemptIfCurrentWordChanged(AtomicBoolean isChanged) {
        if (!isChanged.get()) {
            attempt--;
        }
    }

    public boolean canGuess() {
        return attempt > 0;
    }

    public boolean isGuessed() {
        return currentWord.equals(wordToGuess);
    }

    public void guessWord(String word) throws GivenWordSizeException {
        if (word.length() != wordToGuess.length()) {
            throw new GivenWordSizeException("The given word is not the same size than to word to guess");
        }
        if (word.toUpperCase().equals(wordToGuess)) {
            currentWord = wordToGuess;
        } else {
            attempt--;
        }
    }
}
