package fr.mishii.hangman.core.entity;

public enum Level {
    EASY, MEDIUM, DIFFICULT, NOT_MANAGE
}
