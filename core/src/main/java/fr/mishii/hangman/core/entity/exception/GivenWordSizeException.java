package fr.mishii.hangman.core.entity.exception;

public class GivenWordSizeException extends Exception {
    public GivenWordSizeException(String message) {
        super(message);
    }
}
