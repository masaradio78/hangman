package fr.mishii.hangman.core.utils;

public interface Logger {
    void out(String message);

    void flush();
}
