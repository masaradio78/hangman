package fr.mishii.hangman.core.utils;

import java.util.Random;

public class RandomHelperImpl implements RandomHelper {
    @Override
    public int getRandomInt(int min, int max) {
        var random = new Random();
        return random.nextInt(max - min) + min;
    }
}
