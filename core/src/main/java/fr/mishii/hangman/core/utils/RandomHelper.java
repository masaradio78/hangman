package fr.mishii.hangman.core.utils;

public interface RandomHelper {
    int getRandomInt(int min, int max);
}
