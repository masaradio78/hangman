# Hangman

The game hangman with hexagonal architecture, or trying to do in this way

### Requirements

- Java 11
- Maven

### Available on

#### -> CLI

Enter this command to play the game

```
mvn clean package
java -jar framework/cli/target/cli-1.0-SNAPSHOP-shaded.jar
```