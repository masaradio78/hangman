package fr.mishii.hangman.framework.cli.utils;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class InputImplTest {

    @AfterEach
    void tearDown() {
        System.setIn(System.in);
    }

    @Test
    public void inputString_shouldReturnStringWhenSystemReceiveString() {
        String input = "test";
        InputStream in = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        System.setIn(in);

        var sut = new InputImpl();

        var result = sut.inputString();
        assertThat(result).isEqualTo(input);
    }

    @Test
    public void inputChar_shouldReturnOneCharWhenSystemReceiveString() {
        String input = "test";
        InputStream in = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        System.setIn(in);

        var sut = new InputImpl();

        var result = sut.inputChar();
        assertThat(result).isEqualTo(input.charAt(0));
    }
}