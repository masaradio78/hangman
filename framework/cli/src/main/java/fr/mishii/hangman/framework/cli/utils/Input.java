package fr.mishii.hangman.framework.cli.utils;

public interface Input {
    Integer inputInt();

    String inputString();

    Character inputChar();
}
