package fr.mishii.hangman.framework.cli.utils;

import fr.mishii.hangman.core.utils.Logger;

public class LoggerImpl implements Logger {
    @Override
    public void out(String message) {
        System.out.println(message);
    }

    @Override
    public void flush() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
}
