package fr.mishii.hangman.framework.cli.utils;

import java.util.Scanner;

public class InputImpl implements Input {

    private final Scanner scanner;

    public InputImpl() {
        this.scanner = new Scanner(System.in);
    }

    @Override
    public String inputString() {
        return scanner.next();
    }

    @Override
    public Character inputChar() {
        return scanner.next().charAt(0);
    }

    @Override
    public Integer inputInt() {
        return Integer.parseInt(scanner.next());
    }
}
