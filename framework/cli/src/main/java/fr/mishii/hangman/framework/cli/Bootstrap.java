package fr.mishii.hangman.framework.cli;

import fr.mishii.hangman.core.config.WordConfig;
import fr.mishii.hangman.core.dao.WordDao;
import fr.mishii.hangman.core.utils.Logger;
import fr.mishii.hangman.dataprovider.config.SimpleWordConfig;
import fr.mishii.hangman.dataprovider.dao.ApiWordDao;
import fr.mishii.hangman.dataprovider.helper.RequestHelper;
import fr.mishii.hangman.dataprovider.helper.RequestHelperImpl;
import fr.mishii.hangman.framework.cli.utils.Input;
import fr.mishii.hangman.framework.cli.utils.InputImpl;
import fr.mishii.hangman.framework.cli.utils.LoggerImpl;
import fr.mishii.hangman.usecase.ChooseAWord;
import fr.mishii.hangman.usecase.GetListWords;

public class Bootstrap {
    public static Game build() {
        WordConfig wordConfig = new SimpleWordConfig();
        RequestHelper<String> requestHelper = new RequestHelperImpl<>();
        WordDao wordDao = new ApiWordDao(requestHelper);
        var getListWords = new GetListWords(wordConfig, wordDao);

        var chooseAWord = new ChooseAWord();

        Logger logger = new LoggerImpl();

        Input input = new InputImpl();

        return new Game(getListWords, chooseAWord, logger, input);
    }
}
