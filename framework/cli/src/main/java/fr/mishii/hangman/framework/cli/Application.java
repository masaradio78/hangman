package fr.mishii.hangman.framework.cli;

public class Application {
    public static void main(String[] args) {
        var game = Bootstrap.build();

        game.execute();
    }
}
