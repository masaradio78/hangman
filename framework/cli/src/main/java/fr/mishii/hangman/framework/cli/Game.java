package fr.mishii.hangman.framework.cli;

import fr.mishii.hangman.core.entity.Hangman;
import fr.mishii.hangman.core.entity.Level;
import fr.mishii.hangman.core.entity.exception.GivenWordSizeException;
import fr.mishii.hangman.core.utils.Logger;
import fr.mishii.hangman.framework.cli.utils.Input;
import fr.mishii.hangman.usecase.ChooseAWord;
import fr.mishii.hangman.usecase.GetListWords;
import fr.mishii.hangman.usecase.exceptions.EmptyWordListException;
import fr.mishii.hangman.usecase.exceptions.NotManagedLevelException;
import fr.mishii.hangman.usecase.exceptions.NotWordsForCurrentLevelException;

import java.io.IOException;
import java.util.HashMap;
import java.util.function.Consumer;

public class Game {
    private final GetListWords getListWords;
    private final ChooseAWord chooseAWord;
    private final Logger logger;
    private final Input input;
    private final HashMap<Character, Level> mapLevel;
    private final HashMap<Character, Consumer<Hangman>> mapSelection;

    public Game(GetListWords getListWords, ChooseAWord chooseAWord, Logger logger, Input input) {
        this.getListWords = getListWords;
        this.chooseAWord = chooseAWord;
        this.logger = logger;
        this.input = input;

        mapLevel = new HashMap<>();
        mapLevel.put('1', Level.EASY);
        mapLevel.put('2', Level.MEDIUM);
        mapLevel.put('3', Level.DIFFICULT);

        mapSelection = new HashMap<>();
        mapSelection.put('1', this::guessCharacterOfWord);
        mapSelection.put('2', this::guessWord);
    }

    public void execute() {
        boolean stop = false;
        try {
            while (!stop) {
                start();
                stop = stopOrNotTheGame();
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Problem to get list of words");
        }
    }

    private void start() throws IOException {
        int attemptNumber = getAttemptNumber();
        Level level = chooseLevel();
        String word = getWord(level);

        var hangman = new Hangman(word, attemptNumber);

        while (hangman.canGuess() && !hangman.isGuessed()) {
            logger.flush();
            logger.out("Attempt remain: " + hangman.getAttempt());
            printCurrentWord(hangman);
            guessOneCharacterOrWord(hangman);
        }

        logger.flush();
        if (hangman.isGuessed()) {
            logger.out("You win hangman game.");
        } else {
            logger.out("You loose the hangman game.");
        }
        logger.out("The word is " + hangman.getWordToGuess());
    }

    private int getAttemptNumber() {
        int attemptNumber = 0;
        logger.flush();
        while (attemptNumber == 0) {
            logger.out("Choose the number of attempt");
            try {
                attemptNumber = input.inputInt();
            } catch (Exception ignored) {
                logger.flush();
                logger.out("The attempt has to be an integer");
            }
            if (attemptNumber <= 0) {
                attemptNumber = 0;
            }
        }
        return attemptNumber;
    }

    private boolean stopOrNotTheGame() {
        boolean stop;
        logger.out("Stop the game, Yes => 'Y', No : Other input");
        stop = Character.toUpperCase(input.inputChar()) == 'Y';
        return stop;
    }

    private void printCurrentWord(Hangman hangman) {
        logger.out("The current word : ");
        var currentWord = hangman.getCurrentWord();

        StringBuilder currentWordToDisplay = new StringBuilder();
        for (int i = 0; i < currentWord.length(); i++) {
            currentWordToDisplay.append(currentWord.charAt(i)).append(" ");
        }
        logger.out(currentWordToDisplay.toString());
    }

    private void printChooseLevel() {
        logger.flush();
        logger.out("Choose your level :");
        logger.out("1. EASY");
        logger.out("2. MEDIUM");
        logger.out("3. DIFFICULT");
    }

    private Level chooseLevel() {
        printChooseLevel();
        var numberLevel = input.inputChar();

        if (!mapLevel.containsKey(numberLevel)) return Level.NOT_MANAGE;

        return mapLevel.get(numberLevel);
    }

    private String getWord(Level level) throws IOException {
        String word = null;
        while (word == null) {
            try {
                var listWord = getListWords.execute();
                word = chooseAWord.execute(listWord, level);
            } catch (NotWordsForCurrentLevelException e) {
                System.err.println("Can't found a word in list word for the level : " + level);
            } catch (NotManagedLevelException e) {
                System.err.println("Choosen level is incorrect");
            } catch (EmptyWordListException e) {
                System.out.println("Can't choose a word for empty list");
            }
        }
        return word;
    }

    private void guessOneCharacterOrWord(Hangman hangman) {
        Character selection = null;
        while (selection == null) {
            selection = chooseSelection();
            if (!mapSelection.containsKey(selection)) {
                logger.out("Choose 1 or 2");
                selection = null;
            } else {
                mapSelection.get(selection).accept(hangman);
            }
        }
    }

    private void guessCharacterOfWord(Hangman hangman) {
        logger.out("Your character :");
        var character = input.inputChar();
        hangman.guessIfOneCharacterIsInWord(character);
    }

    private void guessWord(Hangman hangman) {
        String attemptedWord = null;
        while (attemptedWord == null) {
            try {
                logger.out("Select the word :");
                attemptedWord = input.inputString();
                hangman.guessWord(attemptedWord);
            } catch (GivenWordSizeException e) {
                System.out.println("Choose a word with length : " + hangman.getCurrentWord().length());
            }
        }
    }

    private Character chooseSelection() {
        logger.out("Would you want to choose a character or figure out the word ?");
        logger.out("1. Choose a character");
        logger.out("2. Choose a word");
        return input.inputChar();
    }
}
