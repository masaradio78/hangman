package fr.mishii.hangman.dataprovider.helper;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class RequestHelperImpl<T> implements RequestHelper<T> {

    @Override
    public List<T> getAll(String urlToSend) throws IOException {
        URL url = new URL(urlToSend);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("accept", "application/json");
        InputStream responseStream = connection.getInputStream();

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(responseStream, new TypeReference<>() {
        });
    }
}
