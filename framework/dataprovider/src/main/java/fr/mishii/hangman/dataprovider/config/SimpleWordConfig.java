package fr.mishii.hangman.dataprovider.config;

import fr.mishii.hangman.core.config.WordConfig;

public class SimpleWordConfig implements WordConfig {
    @Override
    public Integer getNumberWordsRequest() {
        return 15;
    }
}
