package fr.mishii.hangman.dataprovider.dao;

import fr.mishii.hangman.core.dao.WordDao;
import fr.mishii.hangman.dataprovider.helper.RequestHelper;

import java.io.IOException;
import java.util.List;

public class ApiWordDao implements WordDao {
    public static String WORD_API_URL_NUMBER = "https://random-word-api.herokuapp.com/word?number=";

    private final RequestHelper<String> requestHelper;

    public ApiWordDao(RequestHelper<String> requestHelper) {
        this.requestHelper = requestHelper;
    }

    @Override
    public List<String> getWords(int numberWords) throws IOException {
        String urlWithAdditionalNumberWordsData = WORD_API_URL_NUMBER + numberWords;
        return requestHelper.getAll(urlWithAdditionalNumberWordsData);
    }
}
