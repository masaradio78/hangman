package fr.mishii.hangman.dataprovider.helper;

import java.io.IOException;
import java.util.List;

public interface RequestHelper<T> {
    List<T> getAll(String url) throws IOException;
}
