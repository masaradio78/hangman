package fr.mishii.hangman.dataprovider.dao;

import fr.mishii.hangman.dataprovider.helper.RequestHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ApiWordDaoTest {
    private ApiWordDao sut;

    @Mock
    RequestHelper<String> mockRequestHelper;

    @BeforeEach
    public void setup() {
        sut = new ApiWordDao(mockRequestHelper);
    }

    @Test
    public void should_call_requestHelper_to_get_list_words() throws IOException {
        var numberWords = 5;

        sut.getWords(numberWords);

        verify(mockRequestHelper, times(1))
                .getAll(ApiWordDao.WORD_API_URL_NUMBER + numberWords);
    }

    @Test
    public void given_number_words_3_should_return_3_words() throws IOException {
        var numberWords = 3;
        var expectedWords = Arrays.asList("Toto", "tatata", "ti");
        when(mockRequestHelper.getAll(ApiWordDao.WORD_API_URL_NUMBER + numberWords)).thenReturn(expectedWords);

        var result = sut.getWords(numberWords);

        assertThat(result).isNotNull();
        assertThat(result.size()).isEqualTo(numberWords);
        assertThat(result).isEqualTo(expectedWords);
    }

}