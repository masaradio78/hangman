package fr.mishii.hangman.usecase;

import fr.mishii.hangman.core.entity.Level;
import fr.mishii.hangman.usecase.exceptions.EmptyWordListException;
import fr.mishii.hangman.usecase.exceptions.NotManagedLevelException;
import fr.mishii.hangman.usecase.exceptions.NotWordsForCurrentLevelException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ExtendWith(MockitoExtension.class)
public class ChooseAWordTest {
    ChooseAWord sut;

    @BeforeEach
    public void setup() {
        sut = new ChooseAWord();
    }

    @DisplayName("Given empty word list should throw exception")
    @Test
    public void given_empty_word_list_should_throw_exception() {
        assertThatThrownBy(() -> {
            sut.execute(new ArrayList<>(), Level.EASY);
        }).isInstanceOf(EmptyWordListException.class)
                .hasMessage("A word can't be choose in empty list");
    }

    @Test
    public void given_word_list_with_not_managed_leve_should_throw_exception() {
        assertThatThrownBy(() -> sut.execute(Collections.singletonList("toto"), Level.NOT_MANAGE))
                .isInstanceOf(NotManagedLevelException.class);
    }

    @DisplayName("Given few words in word list 'tatata', 'toto', 'teeteeteeteetee'")
    @Nested
    public class GivenFewWordsInWordList {
        List<String> listWord;

        @BeforeEach
        void setup() {
            listWord = Arrays.asList("tatata", "toto", "teeteeteeteetee");
        }

        @DisplayName("Given Level.EASY should return 'toto'")
        @Test
        void given_level_easy_should_return_appropriate_word() throws Exception {
            var result = sut.execute(listWord, Level.EASY);
            assertThat(result).isEqualTo("toto");
        }

        @DisplayName("Given Level.MEDIUM should return 'tatata'")
        @Test
        void given_level_medium_should_return_appropriate_word() throws Exception {
            var result = sut.execute(listWord, Level.MEDIUM);
            assertThat(result).isEqualTo("tatata");
        }

        @DisplayName("Given Level.DIFFICULT should return 'teeteeteeteetee'")
        @Test
        void given_level_difficult_should_return_appropriate_word() throws Exception {
            var result = sut.execute(listWord, Level.DIFFICULT);
            assertThat(result).isEqualTo("teeteeteeteetee");
        }
    }

    @DisplayName("Given list word for each word more than 5 characters")
    @Nested
    public class GivenListWordEachWordMoreThan5Char {
        List<String> listWordMoreThan5Char;

        @BeforeEach
        void setup() {
            listWordMoreThan5Char = Arrays.asList("tatata", "tototo", "etetetetetete");
        }

        @DisplayName("When Level is EASY should throw exception")
        @Test
        void when_level_easy_should_throw_exception() {
            assertThatThrownBy(() -> sut.execute(listWordMoreThan5Char, Level.EASY))
                    .isInstanceOf(NotWordsForCurrentLevelException.class)
                    .hasMessage("The list word not contains word for level EASY");
        }

        @DisplayName("When Leve is MEDIUM shoudl get 'tatata' word")
        @Test
        void when_level_medium_should_get_tatata() throws EmptyWordListException, NotWordsForCurrentLevelException, NotManagedLevelException {
            var result = sut.execute(listWordMoreThan5Char, Level.MEDIUM);
            assertThat(result).isEqualTo("tatata");
        }
    }

    @DisplayName("Given list word for each word less than 6 or more than 12 characters")
    @Nested
    public class GivenListWordEachWordLessThan6OrMoreThan12 {
        List<String> listWordLessThan6OrMoreThan12Char;

        @BeforeEach
        void setup() {
            listWordLessThan6OrMoreThan12Char = Arrays.asList("Toto", "Tatatatatatata", "Titititititititi");
        }

        @DisplayName("When Level is MEDIUM should throw exception")
        @Test
        void when_level_medium_should_throw_exception() {
            assertThatThrownBy(() -> sut.execute(listWordLessThan6OrMoreThan12Char, Level.MEDIUM))
                    .isInstanceOf(NotWordsForCurrentLevelException.class)
                    .hasMessage("The list word not contains word for level MEDIUM");
        }

        @DisplayName("When Level is DIFFICULT should get 'Tatatatatatata'")
        @Test
        void when_level_difficult_should_get_Tatatatatatata() throws EmptyWordListException, NotWordsForCurrentLevelException, NotManagedLevelException {
            var result = sut.execute(listWordLessThan6OrMoreThan12Char, Level.DIFFICULT);
            assertThat(result).isEqualTo("Tatatatatatata");
        }
    }
}