package fr.mishii.hangman.usecase;

import fr.mishii.hangman.core.config.WordConfig;
import fr.mishii.hangman.core.dao.WordDao;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class GetListWordsTest {
    GetListWords sut;

    @Mock
    WordConfig wordConfig;

    @Mock
    WordDao wordDao;

    @BeforeEach
    public void setup() {
        sut = new GetListWords(wordConfig, wordDao);
    }

    @Test
    public void should_get_number_words_to_request() throws IOException {
        sut.execute();
        verify(wordConfig, times(1)).getNumberWordsRequest();
    }

    @Test
    public void should_send_request_to_get_list_words() throws IOException {
        var numberWords = 6;
        when(wordConfig.getNumberWordsRequest()).thenReturn(numberWords);

        sut.execute();

        verify(wordDao, times(1)).getWords(numberWords);
    }

    @Test
    public void should_get_list_words() throws IOException {
        var numberWords = 3;
        var expectedListWords = Arrays.asList("toto", "tata", "titi");
        when(wordConfig.getNumberWordsRequest()).thenReturn(numberWords);
        when(wordDao.getWords(numberWords)).thenReturn(expectedListWords);

        var result = sut.execute();

        assertThat(result).isEqualTo(expectedListWords);
    }
}