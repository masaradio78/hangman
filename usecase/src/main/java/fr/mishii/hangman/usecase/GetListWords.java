package fr.mishii.hangman.usecase;

import fr.mishii.hangman.core.config.WordConfig;
import fr.mishii.hangman.core.dao.WordDao;

import java.io.IOException;
import java.util.List;

public class GetListWords {
    private final WordConfig wordConfig;
    private final WordDao wordDao;

    public GetListWords(WordConfig wordConfig, WordDao wordDao) {
        this.wordConfig = wordConfig;
        this.wordDao = wordDao;
    }

    public List<String> execute() throws IOException {
        var numberWords = wordConfig.getNumberWordsRequest();
        return wordDao.getWords(numberWords);
    }
}
