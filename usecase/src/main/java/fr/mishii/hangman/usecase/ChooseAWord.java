package fr.mishii.hangman.usecase;


import fr.mishii.hangman.core.entity.Level;
import fr.mishii.hangman.usecase.exceptions.EmptyWordListException;
import fr.mishii.hangman.usecase.exceptions.NotManagedLevelException;
import fr.mishii.hangman.usecase.exceptions.NotWordsForCurrentLevelException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class ChooseAWord {

    private final Map<Level, Function<String, Boolean>> mapLevelFunctions;
    private static final int MIN_LENGTH_EASY = 1;
    private static final int MAX_LENGTH_EASY = 5;
    private static final int MIN_LENGTH_MEDIUM = 6;
    private static final int MAX_LENGTH_MEDIUM = 12;
    private static final int MIN_LENGTH_DIFFICULT = 13;
    private static final int MAX_LENGTH_DIFFICULT = 20;

    public ChooseAWord() {
        mapLevelFunctions = new HashMap<>();
        mapLevelFunctions.put(Level.EASY, (word) -> isWordBetweenMinAndMax(word, MIN_LENGTH_EASY, MAX_LENGTH_EASY));
        mapLevelFunctions.put(Level.MEDIUM, (word) -> isWordBetweenMinAndMax(word, MIN_LENGTH_MEDIUM, MAX_LENGTH_MEDIUM));
        mapLevelFunctions.put(Level.DIFFICULT, (word) -> isWordBetweenMinAndMax(word, MIN_LENGTH_DIFFICULT, MAX_LENGTH_DIFFICULT));
    }

    private boolean isWordBetweenMinAndMax(String word, int minLength, int maxLength) {
        return word.length() >= minLength && word.length() <= maxLength;
    }

    public String execute(List<String> words, Level level)
            throws EmptyWordListException, NotManagedLevelException, NotWordsForCurrentLevelException {
        checkIfWordsIsNotEmpty(words);
        checkIfLevelIsPresent(level);

        return getWordDependToLevel(words, level);
    }

    private void checkIfWordsIsNotEmpty(List<String> words) throws EmptyWordListException {
        if (words.isEmpty()) {
            throw new EmptyWordListException("A word can't be choose in empty list");
        }
    }

    private void checkIfLevelIsPresent(Level level) throws NotManagedLevelException {
        if (!mapLevelFunctions.containsKey(level)) {
            throw new NotManagedLevelException();
        }
    }

    private String getWordDependToLevel(List<String> words, Level level) throws NotWordsForCurrentLevelException {
        var optionalWord = words.stream()
                .filter(word -> mapLevelFunctions.get(level).apply(word))
                .findFirst();
        if (optionalWord.isEmpty()) {
            throw new NotWordsForCurrentLevelException("The list word not contains word for level " + level);
        }

        return optionalWord.get();
    }
}
