package fr.mishii.hangman.usecase.exceptions;

public class NotWordsForCurrentLevelException extends Exception {
    public NotWordsForCurrentLevelException(String message) {
        super(message);
    }
}
