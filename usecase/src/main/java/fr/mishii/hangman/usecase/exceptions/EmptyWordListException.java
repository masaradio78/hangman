package fr.mishii.hangman.usecase.exceptions;

public class EmptyWordListException extends Exception {
    public EmptyWordListException(String message) {
        super(message);
    }
}
